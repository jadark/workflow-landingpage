'use strict';
var gulp        = require('gulp'),
    cache       = require('gulp-cache'),
    browserSync = require('browser-sync').create(),
    reload      = browserSync.reload,
    sass        = require('gulp-sass'),
    jade        = require('gulp-jade'),
    concat      = require('gulp-concat'),
    coffee      = require('gulp-coffee'),
    uglify      = require('gulp-uglify'),
    es          = require('event-stream'),
    plumber     = require('gulp-plumber'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    prefix      = require('gulp-autoprefixer'),
    gutil       = require('gulp-util'),
    sourcemaps  = require('gulp-sourcemaps'),
    entorno  = gutil.env || "prod";


gulp.task('clear', function (done) {
  cache.clearAll(done);
});

gulp.task('javascript', ['clear'], function() {
  es.merge(
    gulp.src('source/coffee/*.coffee')
        .pipe(plumber())
        .pipe(coffee({bare: true})),
    gulp.src('source/javascript/*.js'))
        .pipe(plumber())
        .pipe(uglify({
          compress:{
            drop_console: false
          }
         }))
        .pipe(concat('bundle.js'))
        .pipe( gulp.dest('dist/js/'))
        .pipe(browserSync.reload({stream: true}));
});


gulp.task('sass', ['clear'], function () {
  gulp.src('./source/sass/*.scss')
      .pipe(plumber())
      //.pipe(sourcemaps.init())
      .pipe(sass({
          // outputStyle:'compressed',
        }))
      .pipe(prefix({browsers: ['last 2 versions', 'ie 8', 'ie 9', '> 1%', 'Firefox >= 20', 'Opera 12.1','iOS 7'], cascade: false}))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./dist/css'))
      .pipe(browserSync.reload({stream: true}));
});

gulp.task('templates', ['clear'], function() {
  gulp.src('./source/layouts/*.jade')
      .pipe(jade({
        pretty: true
      }))
      .pipe(gulp.dest('./dist'))
      .pipe(browserSync.reload({stream: true}));
});

gulp.task('images', function() {
  gulp.src('./source/images/*.{png,jpg,jpeg,gif,svg}')
      .pipe(imagemin({
        progressive: true,
        interlaced: true,
        optimizationLevel: 3,
        svgoPlugins: [{
          removeViewBox: false
        }],
        use: [pngquant({
          quality: '75-80',
          speed: 4
        })]
      }))
      .pipe(gulp.dest('dist/images/'));
});
// gulp.task('fonts', function() {
//   gulp.src(['source/fonts/**/*.{eot,svg,ttf,woff,woff2}'])
//       .pipe(gulp.dest('dist/fonts/'));
// });

gulp.task('plugins', function() {
    gulp.src(
          [
            './source/plugins/jquery.min.js',
            './source/plugins/fix_liferay.js',
            './source/plugins/owl.carousel.min.js',
            './source/plugins/bootstrap.min.js'
          ]
        )
        .pipe(concat('vendor_all.js') )
        .pipe(uglify())
        .pipe( gulp.dest('dist/js/vendor'))
        .pipe(browserSync.reload({stream: true}));
});


gulp.task('watch', function () {
  gulp.watch('./source/sass/*.scss', ['sass']);
  gulp.watch('./source/coffee/*.js',  ['javascript']);
  gulp.watch( 'source/layouts/**/*.jade', ['templates'] );
  gulp.watch( './source/javascript/plugins/*.js', ['plugins'] );
  gulp.watch('dist/*.html').on('change', reload);
});

// Static server
gulp.task('serve', function() {
  browserSync.init({
    open: false,
    server: './dist',
    port: 3000
  });
});

var taskDefault = [
    'templates',
    'javascript',
    'plugins',
    // 'fonts',
    'sass',
    'watch',
    'serve',
    'images'
];

gulp.task('default', taskDefault);
